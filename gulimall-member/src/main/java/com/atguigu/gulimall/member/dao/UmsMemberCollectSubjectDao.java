package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.UmsMemberCollectSubjectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的专题活动
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-07-17 13:55:55
 */
@Mapper
public interface UmsMemberCollectSubjectDao extends BaseMapper<UmsMemberCollectSubjectEntity> {
	
}
