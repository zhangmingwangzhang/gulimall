package com.atguigu.gulimall.member.dao;

import com.atguigu.gulimall.member.entity.UmsMemberCollectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的商品
 * 
 * @author chenshun
 * @email sunlightcs@gmail.com
 * @date 2021-07-17 13:55:55
 */
@Mapper
public interface UmsMemberCollectSpuDao extends BaseMapper<UmsMemberCollectSpuEntity> {
	
}
