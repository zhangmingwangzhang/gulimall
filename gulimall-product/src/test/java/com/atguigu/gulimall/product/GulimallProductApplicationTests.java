package com.atguigu.gulimall.product;

import com.atguigu.gulimall.product.entity.BrandEntity;
import com.atguigu.gulimall.product.service.BrandService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

@SpringBootTest

class GulimallProductApplicationTests {
    @Autowired
    BrandService brandService;
    @Test
    void contextLoads() {
//        BrandEntity brandEntity = new BrandEntity();
//        brandEntity.setDescript("1111");
//        brandEntity.setLogo("8s");
//        brandEntity.setName("nihao");
//        brandEntity.setShowStatus(1);
//        brandEntity.setSort(2);
//        boolean save = brandService.save(brandEntity);
//        System.out.println(save);
        List<BrandEntity> list = brandService.list();
        System.out.println(list);
        list.forEach((item)-> {
            System.out.println("=============="+item);
        });
    }

}
